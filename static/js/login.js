$(document).ready(function(){
	var handle_login = function(login_container) {
		username = login_container.find(".login-user").val();
		password = login_container.find(".login-pass").val();

		$.post("/auth", {"action": "login", "user": username, "pass": password}, function(response) {
			if (response.status === "successful") {
				location.reload();
			} else {
				login_container.find(".login-message").text("Authentication failed, check credentials");
			}
		}).fail(function() {
			login_container.find(".login-message").text("Authentication failed, check credentials");
		});
	}

	$(".login-submit").on("click", function(e) {
		login_container = $(e.target).closest(".login");
		handle_login(login_container);
	})

	$(".login-user, .login-pass").keypress(function(e) {
		if (e.which == 13) {	//Enter pressed
			login_container = $(e.target).closest(".login");
			handle_login(login_container);
		}
	})
})
