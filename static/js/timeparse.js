"use strict";

var timeparse = {};

timeparse.relative = function(a, b, dosuffix) {
	//determines when a occured/occurs in relation to b
	//suffix defaults to true
	if (!b) {
		b = new Date();
	}

	//determine suffix and offset (in ms)
	var suffix;
	var difference;
	if (a > b) {
		//a is in b's future
		suffix = "from now";
		difference = a - b;
	} else if (a < b) {
		//a in in b's past
		suffix = "ago";
		difference = b - a;
	} else {
		//a and b must be identical
		return "just now";
	}

	//determine unit to use out of seconds, minutes, hours, days
	var cuttoffSeconds = 59999; //one minute - 1 millisecond
	var cuttoffMinutes = 3599999;
	var cuttoffHours = 86399999;

	//division factor
	var divSeconds = 1000;
	var divMinutes = 60000;
	var divHours = 3600000;
	var divDays = 86400000;

	var number;
	var unit;
	if (difference <= cuttoffSeconds) {
		unit = "second";
		number = difference / divSeconds;
	} else if (difference <= cuttoffMinutes) {
		unit = "minute";
		number = difference / divMinutes;
	} else if (difference <= cuttoffHours) {
		unit = "hour";
		number = difference / divHours;
	} else {
		unit = "day";
		number = difference / divDays;
	}

	if (number < 1) {
		number = 1;
	} else {
		number = Math.floor(number);
	}

	if (number > 1) {
		unit += "s";
	}

	var str = number + " " + unit;

	if (typeof(dosuffix) == "undefined" || dosuffix) {
		str += " " + suffix
	}

	return str;
}
