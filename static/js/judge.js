$(document).ready(function() {
	$("body").on("click", function(e) {
		target = $(e.target);
		if (target.attr("data-link") !== undefined) {
			handleLink(target.attr("data-link"), target);
		} else {
			parents = target.parents();
			parents_length = parents.length;

			for (var i = 0; i < parents_length; i++) {
				if ($(parents[i]).attr("data-link") !== undefined) {
					handleLink($(parents[i]).attr("data-link"), target);
					break;
				}
			}
		}
	});

	$("#team-select").chosen({
		search_contains: true,
		width: "300px"
	}).on("change", function(e) {
		selectTeam(e.target.value);
	});

	getUpdatedData();
})

// Global variables
ev = {
	data: {
		teams: null,
		results: null,
		meta: null,
	},
	link_handlers: {
		view: selectView,
		updateField: sendFieldUpdate,
		editTeam: editTeam,
		updateTeam: updateTeam,
		deleteTeam: deleteTeam,
		confirmDeleteTeam: confirmDeleteTeam,
	},
	update: null,
	selected_category: null,
	selected_team: null,
	selected_view: null,
}

function handleLink(link_raw, element) {
	var link = parseLink(link_raw)
	ev.link_handlers[link.handler_name](link.data, element);
}

function parseLink(link) {
	var link_split = link.split(/^([^:]+):(.+)/);

	return {
		handler_name: link_split[1],
		data: link_split[2],
	};
}

function getUpdatedData() {
	$.get("/teams", function(response) {
		var team_data = {};
		response.forEach(function(team) {
			team_data[team.team_id] = team;
		})

		ev.data.teams = team_data;
		$.get("/scores", function(response) {
			ev.data.results = response.results;
			ev.data.meta = response.meta;

			updateCategoryList();

			if (ev.selected_view == "category" && ev.selected_team !== null) {
				updateTeamResults();
			}

			if (ev.selected_view == "teams") {
				viewTeams();
			}
		});
	});
}

function updateCategoryList() {
	new_list = $("<ul>");
	for (var category_name in ev.data.meta) {
		var element = $("<li>")
			.attr("data-link", "view:category:" + category_name)
			.html(ev.data.meta[category_name].display_name);

		if (category_name === ev.selected_category) {
			element.addClass("active");
		}

		element.appendTo(new_list);
	}
	$("#category-list").html(new_list.html());
}

function selectView(link, element) {
	ev.selected_view = null;
	link_cat = parseLink(link);

	if (link_cat.handler_name === "category") {
		ev.selected_view = "category";
		selectCategory(link_cat.data, element);
		return;
	}

	if (link === "teams") {
		ev.selected_view = "teams";
		viewTeams();
		return;
	}
}

function selectCategory(category_id, element) {
	ev.selected_team = null;
	ev.selected_category = category_id;

	teamDeselected();

	$("#sidebar li").removeClass("active");
	element.addClass("active");

	$("#view-title").html(ev.data.meta[category_id].display_name);

	updateTeamList();

	$(".view").hide()
	$("#view-category").show();
}

function updateTeamList() {
	new_list = $("<select>");
	new_list.append("<option value=\"null\"><option>");

	sorted_team_ids = Object.keys(ev.data.teams);

	console.log(sorted_team_ids);

	sorted_team_ids.sort(function(a, b){
		if (ev.data.teams[a].race_number < ev.data.teams[b].race_number) {
			return -1;
		}
		if (ev.data.teams[a].race_number > ev.data.teams[b].race_number) {
			return 1;
		}
		return 0;
	})

	console.log(sorted_team_ids);

	for (team_id in sorted_team_ids) {
		team_id = parseInt(sorted_team_ids[team_id]);
		var team = ev.data.teams[team_id];
		$("<option>")
			.attr("value", team.team_id)
			.html("{" + team.race_number + "} " + team.name)
			.appendTo(new_list);
	}
	$("#team-select").html(new_list.html());

	$("#team-select").trigger("chosen:updated");
}

function selectTeam(team_id) {
	ev.selected_team = team_id;

	showFields();
	teamSelected();
	updateTeamResults();
}

function showFields() {
	field_container = $("#field-container")
	field_container.html("");

	ev.data.meta[ev.selected_category].fields.forEach(function (field) {
		if (field.type === "Number") {
			$("<div class='field'>")
				.append($("<div class='field-info'>")
					.append(
						$("<span class='field-name'>").text(field.display_name)
					)
					.append(
						$("<span class='field-type'>").text(field.type)
					)
				)
				.append(
					$("<input type='number'>").attr("data-field-name", field.field_name)
				)
				// .append(
				// 	$("<button>")
				// 		.text("Update")
				// 		.attr("data-link", "updateField:" + field.field_name)
				// )
				.appendTo(field_container);
		}
	})

	field_container.append("<button onclick='sendFieldsUpdate()' class='btn-update-all'>Update</button>")
}

function sendFieldsUpdate() {
	fields_elms = $("#field-container [data-field-name]");

	fields = [];

	for (var i = 0; i < fields_elms.length; i++) {
		field = $(fields_elms[i]);
		field_name = field.attr("data-field-name");
		field_value = field.val();

		fields.push({field_name: field_name, data:field_value});
	}

	var post_data = {
		category_name: ev.selected_category,
		team_id: ev.selected_team,
		fields: fields
	};

	$.post("/update", {data: post_data}, getUpdatedData);
}

function sendFieldUpdate(field_name) {
	var field_value = $("input[data-field-name=" + field_name + "]").val();

	var post_data = {
		category_name: ev.selected_category,
		team_id: ev.selected_team,
		fields: [
			{field_name: field_name, data: field_value},
		]
	};

	$.post("/update", {data: post_data}, getUpdatedData);
}

function updateTeamResults() {
	if (ev.data.results[ev.selected_category][ev.selected_team] === undefined) {
		$("#results-current-table").html("Results incomplete, cannot compute!");
		return;
	}

	var table = $("<table>");

	var hdrow = $("<tr>");
	ev.data.meta[ev.selected_category].fields.forEach(function (field) {
		hdrow.append(
			$("<th>").text(field.display_name)
		)
	});
	hdrow.appendTo(table);

	var datarow = $("<tr>")
	ev.data.meta[ev.selected_category].fields.forEach(function (field) {
		datarow.append(
			$("<td>").text(ev.data.results[ev.selected_category][ev.selected_team][field.field_name])
		)
	});
	datarow.appendTo(table);

	$("#results-current-table").html(table.html());
}

function teamSelected() {
	$("#results").show();
}

function teamDeselected() {
	$("#field-container").html("");
	$("#results").hide();
	$("#results-current-table").html("");
}

// Team management

function viewTeams() {
	$("#sidebar li").removeClass("active");
	$("#btn-teams-view").addClass("active");

	$(".view").hide();

	$("#view-title").html("Teams <button onclick='addTeamEdit()'>Add Team</button>");

	view_html = "";

	view_html += "<div id='team-editor'></div>";

	var team_list = $("<div>");
	var teams = ev.data.teams;

	table = $("<table id='team_list'>");
	hdrow = "<tr><th>Name</th><th>Race #</th><th>Class</th><th>Vehicle Type</th><th>Custom Controller</th><th>Members</th></tr>";
	table.append(hdrow);

	for (team_id in teams) {
		var team = teams[team_id];

		datarow = $("<tr>").attr("data-link", "editTeam:" + team_id);

		datarow.append(
			$("<td>").html(team.name)
		)

		datarow.append(
			$("<td>").html(team.race_number)
		)

		datarow.append(
			$("<td>").html(team.class)
		)

		datarow.append(
			$("<td>").html(team.vehicle_type)
		)

		if (team.custom_controller) {
			datarow.append(
				$("<td>").html("Yes")
			)
		} else {
			datarow.append(
				$("<td>").html("No")
			)
		}

		datarow.append(
			$("<td>").html(team.members.replace(/\n/g, "<br />"))
		)

		datarow.appendTo(table);
	}

	table.appendTo(team_list);
	view_html += team_list.html()

	$("#view-management").html(view_html).show();
}

function editTeam(team_id) {
	ev.selected_team = team_id;
	team = ev.data.teams[team_id];

	editor = $("#team-editor");
	editor.html("");

	editor.append("<h3>Editing " + team.name + "</h3>");

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Name"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='name'>").attr("value", team.name))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("RFID Number"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='rfid'>").attr("value", team.rfid))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Race Number"))
				.append($("<span class='field-type'>").html("Number"))
		)
		.append($("<input type='number' data-field-name='race_number'>").attr("value", team.race_number))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Class"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='class'>").attr("value", team.class))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Vehicle Type"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='vehicle_type'>").attr("value", team.vehicle_type))
		.appendTo(editor);

	// Checkbox string
	if (team.custom_controller) {
		checkbox_str = "<input type='checkbox' data-field-name='custom_controller' checked>";
	} else {
		checkbox_str = "<input type='checkbox' data-field-name='custom_controller'>";
	}

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Custom Controller"))
				.append($("<span class='field-type'>").html("Boolean"))
		)
		.append(checkbox_str)
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Members"))
				.append($("<span class='field-type'>").html("Text"))
		)
		.append($("<textarea rows=4 data-field-name='members'>").html(team.members))
		.appendTo(editor);

	editor.append("<button data-link='updateTeam:" + team_id +"'>Update</button><button data-link='deleteTeam:" + team_id +"'>Delete</button>");

}

function updateTeam(team_id) {
	team = {}

	team.team_id = team_id
	team.name = $("input[data-field-name='name']").val();
	team.rfid = $("input[data-field-name='rfid']").val();
	team.race_number = $("input[data-field-name='race_number']").val();
	team.class = $("input[data-field-name='class']").val();
	team.vehicle_type = $("input[data-field-name='vehicle_type']").val();

	if ($("input[data-field-name='custom_controller']").is(":checked")) {
		team.custom_controller = 1;
	} else {
		team.custom_controller = 0;
	}

	team.members = $("textarea[data-field-name='members']").val();

	$.post("/update", {type: "team", operation: "update", data: team}, getUpdatedData);
}

function deleteTeam(team_id) {
	button = $("button[data-link='deleteTeam:" + team_id + "']");
	button.attr("data-link", "confirmDeleteTeam:" + team_id).attr("style", "background: red; color: white;").html("Confirm Delete");
}

function confirmDeleteTeam(team_id) {
	$.post("/update", {type: "team", operation: "delete", data: {team_id: team_id}}, getUpdatedData);
}

function addTeamEdit() {
	editor = $("#team-editor");
	editor.html("");

	editor.append("<h3>Adding Team</h3>");

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Name"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='name'>"))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("RFID Identifier"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='rfid'>"))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Race Number"))
				.append($("<span class='field-type'>").html("Number"))
		)
		.append($("<input type='number' data-field-name='race_number'>"))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Class"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='class'>"))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Vehicle Type"))
				.append($("<span class='field-type'>").html("String"))
		)
		.append($("<input type='text' data-field-name='vehicle_type'>"))
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Custom Controller"))
				.append($("<span class='field-type'>").html("Boolean"))
		)
		.append("<input type='checkbox' data-field-name='custom_controller'>")
		.appendTo(editor);

	$("<div class='field'>")
		.append(
			$("<div class='field-info'>")
				.append($("<span class='field-name'>").html("Members"))
				.append($("<span class='field-type'>").html("Text"))
		)
		.append($("<textarea rows=4 data-field-name='members'>"))
		.appendTo(editor);

	editor.append("<button onclick='addTeam()'>Add Team</button>");
}

function addTeam() {
	team = {}

	team.name = $("input[data-field-name='name']").val();
	team.rfid = $("input[data-field-name='rfid']").val();
	team.race_number = $("input[data-field-name='race_number']").val();
	team.class = $("input[data-field-name='class']").val();
	team.vehicle_type = $("input[data-field-name='vehicle_type']").val();

	if ($("input[data-field-name='custom_controller']").is(":checked")) {
		team.custom_controller = 1;
	} else {
		team.custom_controller = 0;
	}

	team.members = $("textarea[data-field-name='members']").val();

	$.post("/update", {type: "team", operation: "insert", data: team}, getUpdatedData);
}
