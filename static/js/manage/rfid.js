"use strict";

var local = {
	lastUpdate: null,
	uploadQueue: [],
	inProgress: {},
	attentionRequired: [],
}

var race = {
	category: "economy",
	laps: 5,
}

$(document).ready(function(){
	$("#rfid-entry").on("keyup", function(e){
		if (e.keyCode == 13) {
			submitTag();
		}
	}).focus();

	$("#rfid-manual-btn").on("click", submitTag);

	setInterval(function() {
		if (local.uploadQueue.length != 0) {
			local.uploadQueue.forEach(function(current, index) {
				//upload - TO IMPLEMENT
				console.dir(current.data);
				//if successful
				log("Uploading data for " + current.data.id +". Send delay: " + ((new Date() - current.uploadQueued)/1000).toFixed(3) + "s.");
				local.uploadQueue.splice(index, 1);
			});
		}
	}, 1000);
});

function submitTag() {
	var tag = $("#rfid-entry").val();
	$("#rfid-entry").val("");

	time(tag);
}

function upload(id) {
	var toUpload = {
		uploadQueued: new Date(),
		url: "/endpoint/dataupload.php",
		requestType: "update",
		data: local.inProgress[id],
	}
	local.uploadQueue.push(toUpload);
}

function time(id) {
	if (local.inProgress[id]) {
		var time = new Date();
		var obj = local.inProgress[id];

		if (obj.ended) {
			return;
		}

		//record
		obj.lap.push(time);
		obj.currentlap += 1;

		//determing lap time and if is shorter
		if (obj.currentlap == 1 || obj.shortestlap === null) { //if first lap
			var laptime = time - obj.start;
			obj.shortestlap = laptime;
		} else {
			var laptime = time - obj.lap[obj.currentlap - 2]; //-1 for zero index, -1 for previous
			if (laptime < obj.shortestlap) {
				obj.shortestlap = laptime;
			}
		}


		log("Lap " + obj.currentlap + " for " + id + ". Lap time: " + (laptime / 1000).toFixed(3) + "s. Total time: " + ((time - obj.start)/1000).toFixed(3) + "s");

		upload(id);
	} else {
		var time = new Date();
		local.inProgress[id] = {
			id: id,
			start: time,
			lap: [],
			shortestlap: null,
			currentlap: 0, //zero indexed
			ended: false,
			status: "timing",
		};
		log("Start timing of " + id);
	}
}

function log(s) {
	$("#rfid-log").append(getDate() + " | " + s + "\n");
}

//get formatted date
function getDate() {
	var d = new Date;

	var year = d.getFullYear();
	var month = d.getMonth() + 1;
	if (month < 10) {
		month = "0" + month;
	}
	var day = d.getDate();
	if (day < 10) {
		day = "0" + day;
	}
	var datestr = [year, month, day].join('-');

	var hours = d.getHours()
	if (hours < 10) {
		hours = "0" + hours;
	}
	var minutes = d.getMinutes()
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	var milliseconds = d.getMilliseconds()
	if (milliseconds < 10) {
		milliseconds = "00" + milliseconds;
	} else if (milliseconds < 100) {
		milliseconds = "0" + milliseconds;
	}
	var timestr = [hours, minutes, milliseconds].join(':');

	return datestr + " " + timestr;
}
