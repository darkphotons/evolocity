"use strict";

$(document).ready(documentLoaded);

function documentLoaded() {

}

var app = {};
var poll = {};
var results = {};

poll.polls = {};

poll.poll = function(func, timeout, delay, context) {
	var id = this.genid();

	if(!context) {
		context = window;
	}

	if (typeof(func) == "object") { // if arguments supplied
		var f = func[0];
		var args = func[1];
		args.push(id);

		console.log(func);

		this.polls[id] = {id:id, func:f, args:args, timeout:timeout, context:context};

		if (delay) {
			setTimeout(function(id, f, args) {
				f.apply(args);
			}, timeout);
		} else {
			f.apply(context, args);
		}
	} else {
		this.polls[id] = {id:id, func:f, args:false, timeout:timeout};

		if (delay) {
			setTimeout(function(id, func) {
				func(id);
			}, timeout);
		} else {
			func(id);
		}
	}

}

poll.done = function(id) {
	var p = this.polls[id];
	if (!p) { return false; }

	var func = p.func;

	if (p.args) {
		var args = p.args;
		var context = p.context;

		setTimeout(function(id, func, args, context) {
			func.apply(context, args);
		}, p.timeout);
	} else {
		setTimeout(function(id, f) {
			func(id);
		}, p.timeout);
	}
}

poll.genid = function() {
	var id = Math.floor(Math.random() * 10000000000);
	if (this.polls[id]) {
		id = this.genid();
	}
	return id;
}

results.results = {};

results.update = function(data) {
	this.results = data;
}

app.start = function() {
	//start polling
	poll.poll([this.update,
		[{type:'all', num:'10'}, results.update]]
		, 1000, results);
}

app.update = function(data, callback, pollid) {
	console.log(pollid);
	$.post("/data/results.php",
		data,
		function(d, s, j) {
			d = JSON.parse(d);
			if (d.status != 'err') {
				callback(d.message, s, j);
			}
			if (pollid) {
				poll.done(pollid);
			}
		}
	);
}

app.start();
