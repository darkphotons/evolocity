<?php
namespace Evolocity;

/**
 * Routes requests to server to correct resource
 */
class Router {
	/** @var Route[] Array of defined routes */
	private $routes = [];

	/**
	 * Creates and adds a route to the router
	 *
	 * @param string $uri URI to route to
	 * @param callable $handler Function to call when route requested
	 */
	public function add($uri, $handler) {
		$this->routes[] = new Route($uri, $handler);
	}

	/**
	 * Called when a client requests a resource
	 *
	 * @param string $uri URI requested
	 */
	public function route($uri) {
		// Search for route with matching uri
		foreach ($this->routes as $route) {
			if (preg_match("/^" . preg_quote($route->getURI(), "/") . "($|\/.*)/", "/" . $uri)) {
				$route->go();
				return;
			}
		}

		// No route for uri, send 404
		header("HTTP/1.0 404 Not Found");
		echo (new Template("error"))->parse(["error_title" => "404 Not found", "error_description" => "We could not locate a resource at the specified location, please check your link is correct."]);
	}
}

/**
 * Defines a Route
 */
class Route {
	/**
	 * @var string URI to bind route to
	 */
	private $uri;

	/**
	 * @var callable executed when route requested
	 */
	private $handler;

	/**
	 * Constructor for Route
	 *
	 * @param string $uri URI to bind route to
	 * @param callable $handler Executed when route requested
	 */
	public function __construct($uri, $handler) {
		$this->uri = $uri;
		$this->handler = $handler;
	}
	/**
	 * Returns URI the route is bound to
	 *
	 * @return string URI the route is bound to
	 */
	public function getURI() {
		return $this->uri;
	}

	/**
	 * Called when route requested, returns handler
	 *
	 * @return callable Handler for route
	 */
	public function go() {
		$handler = $this->handler;
		return $handler();
	}
}
?>
