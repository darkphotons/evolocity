<?php
namespace Evolocity;

require_once("src/Evolocity/config.php");
require_once("src/Evolocity/template.php");

/**
 * Manages authentication of users
 */
class Auth {
	/**
	 * Initializes auth state information in session
	 */
	public static function init() {
		if (! isset($_SESSION["auth"])) {
			$_SESSION["auth"] = ["state" => "unauthenticated"];
		}
	}

	/**
	 * Returns the current authentication state
	 * @return string Authentication state
	 */
	public static function state() {
		return $_SESSION["auth"]["state"];
	}

	public static function isLevel($level) {
		if (Auth::state() != "authenticated") {
			return False;
		}

		////
		$levelHierarchy = ["admin", "judge"];
		////

		$user_level = $_SESSION["auth"]["level"];
		return array_search($user_level, $levelHierarchy) <= array_search($level, $levelHierarchy);
	}

	/**
	 * Atempts to authenticate a user
	 * @return boolean Whether authentication was successful
	 */
	public static function authenticate($user, $pass) {
		$config = (new Config())["server"]["authentication"];

		$record = ["password_hash" => "none"];
		foreach ($config as $user_record) {
			if ($user_record["user"] == $user) {
				$record = $user_record;
			}
		}

		if(!password_verify($pass, $record["password_hash"])) {
			return False;
		}

		$_SESSION["auth"]["state"] = "authenticated";
		$_SESSION["auth"]["level"] = $record["level"];
		return True;
	}

	/**
	 * Invalidates the authentication of the current session (i.e. log off)
	 * Authentication information is wiped and replaced with initial values
	 */
	public static function invalidate() {
		$_SESSION["auth"] = ["state" => "unauthenticated"];
	}

	/**
	 * Returns true if user has access to the authentication level.
	 *
	 * Returns false if not and:
	 *  - If the user is logged in shows 403 page
	 *  - If the user is not logged in shows 401 page and prompts for authentication
	 */
	public static function challenge($level) {
		// Check for token
		if (isset($_POST["token"])) {
			$config = (new Config())["server"]["auth_tokens"];

			$record = ["level" => "none"];
			foreach ($config as $user_record) {
				if ($user_record["token"] == $_POST["token"]) {
					$record = $user_record;
				}
			}

			if ($record["level"] == "None") {
				return False;
			}

			$levelHierarchy = ["admin", "judge"];
			return array_search($record["level"], $levelHierarchy) <= array_search($level, $levelHierarchy);
		}

		if (Auth::isLevel($level)) {
			return True;
		}

		if (Auth::state() == "authenticated") {
			echo (new Template("error"))->parse([
				"error_title" => "403 Forbidden",
				"error_description" => "You do not have the privleges required to access this resource"
			]);
			return False;
		}

		echo (new Template("error_login"))->parse();
	}
}
?>
