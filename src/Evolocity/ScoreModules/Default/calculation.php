<?php
namespace Evolocity\ScoreModules;

/**
 * Performs a speficied calculation with provided data
 *
 * The calculation is provided as a string. Mathmatical symbols + - * / ( ) are supported.
 * - eg. (2 + 4) * 3
 *
 * Variables can be included in the string, variable names must be only case sensitive alphabetic characters
 * - eg. (2 + count) * multiplier
 *
 * Variables are provided as an array where the key corresponds to the variable name in the calculation.
 *
 * The calculation is programatically calculated and not directly executed and thus unsafe data can be used as variables.
 */
class Calculation implements Field {
	/** @var string The expression to be calculated */
	private $calcstring;

	/** @var float[] Variables to supply to the calculation */
	private $data;

	/** @var boolean Whether the calculation function is at a recursive level */
	private $recursive;

	/** Constructor for calculation
	 *
	 * @param mixed $meta Field type data from category configuration
	 * @param array $data Array of data rows from the database pertaining to the instance of the field
	 * @param array $computed_results Results of previously computed fields
	 *
	 * @return float|int Result of calculation
	 */
	public function __construct($meta, $data, $computed_results) {
		$options = json_decode($meta["options"], True);
		$this->calcstring = $options["calculation"];
		$this->data = $computed_results;
	}

	/**
	 * Called by regex search, returns the value of the specified variable, or an array of values for specified variables
	 *
	 * @param string|string[] $vars Variables to resolve
	 * @return float|float[] Values of variables
	 */
	private function resolve_variable($vars) {
		// If only one value provided by regex
		if (is_string($vars)) {
			return $this->data[$vars];
		}

		if (count($vars) == 1) {
			return $this->data[$vars[0]];
		}

		// Otherwise loop through values
		$out = [];
		for($i = 0, $length = count($vars); $i < $length; $i++) {
			$out[] = $this->data[$vars[$i]];
		}

		return $out;
	}

	/**
	 * Main calculation function
	 *
	 * @param string $calc Calculation to calculate
	 * @return float Result of calculation
	 */
	private function recursive_calculate($calc) {
		if ($this->recursive) {
			$calc = substr($calc, 1, -1);	//recursive, remove brackets
		}

		$this->recursive = true;

		// Search for bracket pairs
		$bracket_location = [];
		$length = strlen($calc);
		$searching = false;
		$bracket_depth = 0;
		$tmp_bracket_location = [];
		for($i = 0; $i < $length; $i++) {
			$char = substr($calc, $i, 1);
			if ($searching) {
				if ($char == "(") {
					$bracket_depth++;
				} else if ($char == ")") {
					$bracket_depth--;

					if ($bracket_depth == 0) {
						$tmp_bracket_location[] = $i + 1;
						$bracket_location[] = $tmp_bracket_location;
						$searching = false;
					}
				}
			} else if ($char == "(") {
				$tmp_bracket_location = [$i];
				$searching = true;
				$bracket_depth = 1;
			}
		}

		// Calculate brackets and replace with result
		$shift = 0;	//Change in calc string length due to replacing bracked substrings with results
		foreach ($bracket_location as $index) {
			$tocalc = substr($calc, $index[0] + $shift, $index[1] + $shift);
			$result = $this->recursive_calculate($tocalc);

			$prelength = strlen($tocalc);
			$postlength = strlen($result);

			$calc = substr($calc, 0, $index[0] + $shift) . $result . substr($calc, $index[1] + $shift);

			$shift += $postlength - $prelength;
		}

		// Resolve variables
		$calc = preg_replace_callback("/[a-zA-Z_]+/", "self::resolve_variable", $calc);

		// Split into operators and values
		$calc = preg_split("/([+\-*\/])/", $calc, -1, PREG_SPLIT_DELIM_CAPTURE);

		// Calculate
		$result = $calc[0];
		for($i = 1, $length = count($calc); $i < $length; $i += $increment) {
			// i: operator, i+1 = value
			$increment = 2;
			$operator = $calc[$i];

			// Negative numbers will be preceeded by an empty string
			// i: operator i+1: '' i+2: '-' i+3: |value|
			if ($calc[$i + 1] == "") {
				$value = 0 - $calc[$i + 3];
				$increment = 4;
			} else {
				$value = $calc[$i + 1];
			}

			// Calculate
			switch ($operator) {
				case "+":
					$result = $result + $value;
					break;
				case "-":
					$result = $result - $value;
					break;
				case "*":
					$result = $result * $value;
					break;
				case "/":
					$result = $result / $value;
					break;
			}
		}
		return $result;
	}

	/**
	 * Public calculation function
	 *
	 * @param float[] $data Data to use for variables in calculation
	 * @return float Result of calculation
	 */
	public function calculate() {
		// Setup
		$calc = $this->calcstring;

		// Remove whitespace
		$calc = preg_replace("/ /", "", $calc);

		// Calculate
		$this->recursive = false;
		return $this->recursive_calculate($calc);
	}

	public function getValue() {
		return $this->calculate();
	}

	public function isComplete() {
		$regex_result = [];
		preg_match("/[a-zA-Z_]+/", $this->calcstring, $regex_result);
		$required_fields = array_slice($regex_result, 1);

		foreach ($required_fields as $field) {
			if (array_key_exists($field, $this->computed_results) == False) {
				return False;
			}
		}

		return True;
	}
}
?>
