<?php
namespace Evolocity\ScoreModules;

class Number implements Field {
	protected $meta;
	protected $data;

	public function __construct($meta, $data, $computed_results) {
		$this->meta = $meta;
		$this->data = $data;
	}

	public function getValue() {
		// Get most recent data entry
		$latest_value = null;
		foreach ($this->data as $data_row) {
			if ($latest_value === null) {
				$latest_value = $data_row;
			} else if ($data_row["timestamp"] > $latest_value["timestamp"]) {
				$latest_value = $data_row;
			}
		}

		return $latest_value["data"];
	}

	public function isComplete() {
		// Return false is there is no data
		if (count($this->data) == 0) {
			return False;
		}

		// Return false if the most recent result is not a number
		if (is_numeric($this->getValue()) === False) {
			return False;
		}

		return True;
	}
}
?>
