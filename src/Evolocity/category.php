<?php
namespace Evolocity;

require_once("src/Evolocity/database.php");

/**
 * Class for managing the creation and access of categories in the database
 */
class Category {
	/**
	 * Lists all categories defined in database
	 * (as list is a reserved keyword _list is used instead)
	 *
	 * Categories are returned as an array of [category_name: x, display_name: y] key value pairs.
	 *
	 * @return array Defined categories
	 */
	public static function _list(){
		$query = new Query(
			"SELECT category_name, display_name FROM categories",
			[],
			["noData" => true]
		);

		return $query->execute();
	}

	/** @var string The name of the category the Query instance is bound to */
	private $category_name;

	/**
	 * Contructor for Query
	 * @param string $category The name of the category to bind the instance to
	 */
	public function __construct($category){
		$this->category_name = $category;
	}

	/**
	 * Returns the current definition for the category
	 * @return array Category definition
	 */
	public function getMeta() {
		// Get category definition
		$category_query = new Query(
			"SELECT category_name, display_name, sort_field, sort_direction, display_order FROM categories
			 WHERE category_name = ?
			 LIMIT 1",
			["s", $this->category_name]
		);
		$category_query_result = $category_query->execute();
		$meta = $category_query_result[0]; // Only take the first result

		// Get all field definitions
		$field_query = new Query(
			"SELECT category_name, field_name, display_name, display_order, type, options
			 FROM category_fields
			 WHERE category_name = ?
			 ORDER BY compute_order ASC",
			 ["s", $this->category_name]
		);
		$meta["fields"] = $field_query->execute();

		return $meta;
	}

	public function getResults() {

		$data_query = new Query(
			"SELECT data_id, timestamp, field_name, team_id, data
			 FROM data
			 WHERE category_name = ?",
			 ["s", $this->category_name]
		);

		$data = $data_query->execute();

		return ResultSet::getResults($data, $this->getMeta());
	}
}

class ResultSet {
	public static function getResults($data, $meta) {
		// Group rows by team
		$data_by_team = [];
		foreach ($data as $row) {
			$team_id = $row["team_id"];
			if (!isset($data_by_team[$team_id])) {
				$data_by_team[$team_id] = [];
			}
			$data_by_team[$row["team_id"]][] = $row;
		}

		//Get result for each team
		$results = [];
		foreach ($data_by_team as $team_id => $data_set) {
			$team_results = [];
			foreach ($meta["fields"] as $field_meta) {
				$field_name = $field_meta["field_name"];
				$field = new ResultSetField($field_meta, $data_set, $team_results);

				if ($field->isComplete() === False) {
					// There is no value for the field
					// Thus the result for the team cannot be displayed
					continue 2; //Skip to next team

				}

				$team_results[$field_name] = $field->getValue();
			}
			$results[$team_id] = $team_results;
		}

		return $results;
	}
}

class ResultSetField {
	private $data;
	private $meta;
	private $computed_results;

	public function __construct($meta, $data, $computed_results) {
		// Get only data that corresponsds to field
		$data_field = [];
		foreach ($data as $row) {
			if ($row["field_name"] == $meta["field_name"]) {
				$data_field[] = $row;
			}
		}

		$this->data = $data;
		$this->meta = $meta;
		$this->computed_results = $computed_results;
	}

	public function isComplete() {
		$field = $this->getField();
		return $field->isComplete();
	}


	public function getValue() {
		$field = $this->getField();
		return $field->getValue();
	}

	protected function getField() {
		$field_name = $this->meta["field_name"];
		$data_rows = [];
		foreach ($this->data as $row) {
			if ($row["field_name"] == $field_name) {
				$data_rows[] = $row;
			}
		}

		$field_type = $this->meta["type"];
		$field_class_name = "Evolocity\\ScoreModules\\" . $field_type;

		return new $field_class_name($this->meta, $data_rows, $this->computed_results);
	}
}
?>
