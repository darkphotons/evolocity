<?php
/*
	Evolovity Score Website
	By Ben Hollows
	GNU General Public License (see ./LICENSE)

	This file is called when requesting a non-static file from the webserver
	As it is run at the start of any request all startup code should be put here
*/

namespace Evolocity;

/* --------------------------------
	Import required files
   -------------------------------- */

require_once("src/Evolocity/objects.php");
require_once("src/Evolocity/router.php");
require_once("src/Evolocity/template.php");
require_once("src/Evolocity/scores.php");
require_once("src/Evolocity/render.php");
require_once("src/Evolocity/config.php");
require_once("src/Evolocity/teams.php");
require_once("src/Evolocity/auth.php");

/* --------------------------------
	PHP Setup
   -------------------------------- */

// Set timezone to GMT, all dates are stored at GMT for consistency
date_default_timezone_set("UTC");

// Remove X-Powered-By header for security
header_remove("X-Powered-By");

/* --------------------------------
	Initialize Session
   -------------------------------- */

session_start();
Auth::init();

/* --------------------------------
	Load Configuration
   -------------------------------- */

$config = new Config();
$config->load("server", "server.yaml");
$config->load("categories", "categories.yaml");

/* --------------------------------
	Load ScoreModules
   -------------------------------- */

require_once("src/Evolocity/ScoreModules/field.php");

// Default Modules
$default_modules_file_list = glob("src/Evolocity/ScoreModules/Default/*.php");
foreach ($default_modules_file_list as $module_file) {
	require_once($module_file);
}

// User Modules
$user_modules_file_list = glob("modules/*.php");
foreach ($user_modules_file_list as $module_file) {
	require_once($module_file);
}

/* --------------------------------
	Setup Routes
   -------------------------------- */

$router = new Router();

// Index page
$router->add("/", function() {
	header("HTTP/1.0 200 OK");
	header("Content-Type: text/html");

	echo (new Template("index"))->parse([
		"render" => Render::indexLeaderboards()
	]);
});

// Page for judges to input, view, and modify scores
$router->add("/judge", function(){
	if(Auth::challenge("judge")){
		echo (new Template("judge"))->parse();
	}
});

// Endpoint to get updated scores
$router->add("/scores", function() {
	header("HTTP/1.0 200 OK");
	header("Content-Type: application/json");

	echo json_encode(ScoreInterface::getResults());
});

// Endpoint for team info
$router->add("/teams", function() {
	header("HTTP/1.0 200 OK");
	header("Content-Type: application/json");

	echo json_encode(TeamInterface::getTeams());
});

// Endpoint for Authentication
$router->add("/auth", function() {
	if (isset($_POST["action"])) {
		if ($_POST["action"] == "login") {
			if (Auth::authenticate($_POST["user"], $_POST["pass"])) {
				header("HTTP/1.0 200 OK");
				header("Content-Type: application/json");

				echo "{\"status\": \"successful\"}";
			} else {
				header("HTTP/1.0 401 Unauthorized");
				header("Content-Type: application/json");

				echo "{\"status\": \"failed\"}";
			}
			return;
		} else if ($_POST["action"] == "logout") {
			Auth::invalidate();
			header("HTTP/1.0 200 OK");
			header("Content-Type: application/json");

			echo "{\"status\": \"successful\"}";
			return;
		}
	}
	if (isset($_GET["logout"])) {
		Auth::invalidate();
		header("HTTP/1.0 303 See Other");
		header("Location: /");
		return;
	}
});


$router->add("/update", function() {
	if (isset($_GET["test"])) {
		if ($_GET["test"] == "rfid_endpoint") {
			echo json_encode(["result" => 1]);
			return;
		}

		echo json_encode(["result" => 0]);
		return;
	}

	if(Auth::challenge("judge")){
		switch ($_POST["type"]) {
			case "team":
				echo TeamInterface::updateTeams($_POST["data"], $_POST["operation"]);
				break;

			case "results":
				echo ScoreInterface::updateResults($_POST["data"]);
				break;

			// Legacy case
			default:
				echo ScoreInterface::updateResults($_POST["data"]);
				break;
		}

	}
});


$router->route($_GET["uri"]);
?>
